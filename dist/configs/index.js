"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.JWT_EXPIRED_IN = exports.PASSWORD_SALT = exports.JWT_SECRET = exports.PORT = exports.HOST = void 0;
const dotEnv = __importStar(require("dotenv"));
dotEnv.config();
exports.HOST = process.env.HOST || 'http://localhost';
exports.PORT = process.env.PORT;
exports.JWT_SECRET = process.env.JWT_SECRET;
exports.PASSWORD_SALT = 1;
exports.JWT_EXPIRED_IN = 30 * 24 * 60 * 60;
if (!exports.PORT || !exports.JWT_SECRET) {
    throw new Error('Missing required env');
}
//# sourceMappingURL=index.js.map