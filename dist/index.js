"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const dotenv_1 = __importDefault(require("dotenv"));
dotenv_1.default.config();
const app_1 = __importDefault(require("./app"));
const category_route_1 = __importDefault(require("./handlers/category/category.route"));
const wallpaper_route_1 = __importDefault(require("./handlers/wallpaper/wallpaper.route"));
const app = new app_1.default([
    new category_route_1.default(),
    new wallpaper_route_1.default(),
]);
app.listen();
//# sourceMappingURL=index.js.map