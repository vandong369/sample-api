"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const body_parser_1 = __importDefault(require("body-parser"));
const express_1 = __importDefault(require("express"));
const express_pino_logger_1 = __importDefault(require("express-pino-logger"));
const cors_1 = __importDefault(require("cors"));
const x_hub_signature_1 = require("x-hub-signature");
const response_1 = __importDefault(require("./utils/response"));
const error_middleware_1 = __importDefault(require("./middlewares/error.middleware"));
class App {
    constructor(route) {
        this.app = express_1.default();
        this.initializeMiddleware();
        this.initializeRoutes(route);
        this.initializeErrorHandling();
    }
    listen() {
        this.app.listen(process.env.PORT, () => {
            console.log(`App listening on the port ${process.env.PORT}`);
        });
    }
    initializeMiddleware() {
        this.app.use((req, res, next) => {
            body_parser_1.default.json({
                verify: x_hub_signature_1.middleware.extractRawBody,
            })(req, res, (err) => {
                if (err) {
                    return res.status(400).json({ error: 'Bad json' });
                }
                next();
            });
        });
        this.app.use(body_parser_1.default.urlencoded({ extended: false }));
        this.app.use(express_pino_logger_1.default({ level: 'info' }));
        this.app.use(response_1.default);
        this.app.use(cors_1.default({
            origin: '*',
        }));
    }
    initializeErrorHandling() {
        this.app.use(error_middleware_1.default);
    }
    initializeRoutes(controllers) {
        // this.app.use('/ringtones_data', express.static('public'));
        controllers.forEach((controller) => {
            this.app.use('/wallpaper/v1', controller.router);
        });
    }
}
exports.default = App;
//# sourceMappingURL=app.js.map