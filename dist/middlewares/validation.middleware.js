"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const celebrate_1 = require("celebrate");
function validationMiddleware(schema) {
    return celebrate_1.celebrate(schema);
}
exports.default = validationMiddleware;
//# sourceMappingURL=validation.middleware.js.map