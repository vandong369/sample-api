"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const http_status_1 = __importDefault(require("http-status"));
const lodash_1 = require("lodash");
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const api_error_1 = __importDefault(require("../utils/api-error"));
const configs_1 = require("../configs");
function authMiddleware(req, res, next) {
    return __awaiter(this, void 0, void 0, function* () {
        const token = req.headers.authorization;
        if (!lodash_1.isString(token) || !lodash_1.startsWith(token, 'Bearer ')) {
            return next(new api_error_1.default('AUTHENTICATION_FAILED', http_status_1.default.UNAUTHORIZED));
        }
        try {
            const signedToken = token.split('Bearer ')[1];
            const verificationResponse = yield jsonwebtoken_1.default.verify(signedToken, configs_1.JWT_SECRET);
            req.user = verificationResponse;
            return next();
        }
        catch (err) {
            return next(new api_error_1.default('AUTHENTICATION_FAILED', http_status_1.default.UNAUTHORIZED));
        }
    });
}
exports.default = authMiddleware;
//# sourceMappingURL=auth.middleware.js.map