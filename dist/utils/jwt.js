"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getToken = exports.generateResetPasswordToken = exports.verify = exports.sign = void 0;
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const lodash_1 = require("lodash");
const configs_1 = require("../configs");
exports.sign = (payload, expiresIn = 2592000) => __awaiter(void 0, void 0, void 0, function* () {
    return jsonwebtoken_1.default.sign(payload, configs_1.JWT_SECRET, {
        expiresIn,
    });
});
exports.verify = (token) => __awaiter(void 0, void 0, void 0, function* () { return jsonwebtoken_1.default.verify(token, configs_1.JWT_SECRET); });
exports.generateResetPasswordToken = (payload, expiresIn = 600) => __awaiter(void 0, void 0, void 0, function* () {
    return jsonwebtoken_1.default.sign(payload, configs_1.JWT_SECRET, {
        expiresIn,
    });
});
exports.getToken = (req) => {
    const token = lodash_1.get(req, 'query.token');
    if (token) {
        return token;
    }
    if (req.headers) {
        const authorization = lodash_1.get(req, 'headers.authorization');
        if (!authorization) {
            return '';
        }
        const tokens = authorization.split('Bearer ');
        return lodash_1.get(tokens, '[1]');
    }
    return '';
};
//# sourceMappingURL=jwt.js.map