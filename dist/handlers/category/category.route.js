"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const category_controller_1 = __importDefault(require("./category.controller"));
class CategoryRoute {
    constructor() {
        this.router = express_1.Router();
        this.categoryController = new category_controller_1.default();
        this.initializeRoutes();
    }
    initializeRoutes() {
        this.router.get('/categories', this.categoryController.getCategories);
    }
}
exports.default = CategoryRoute;
//# sourceMappingURL=category.route.js.map