import * as dotEnv from 'dotenv';

dotEnv.config();

export const HOST = (process.env.HOST as string) || 'http://localhost';
export const PORT = process.env.PORT as string;
export const JWT_SECRET = process.env.JWT_SECRET as string;
export const PASSWORD_SALT = 1;
export const JWT_EXPIRED_IN = 30 * 24 * 60 * 60;

if (!PORT || !JWT_SECRET) {
    throw new Error('Missing required env');
}