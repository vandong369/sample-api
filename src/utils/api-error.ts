import HttpStatus from 'http-status';

class ApiError extends Error {
    public status: number;

    constructor(message: string, status = HttpStatus.INTERNAL_SERVER_ERROR) {
        super(message);
        this.status = status;
    }
}

export default ApiError;
