import dotEnv from 'dotenv';

dotEnv.config();
import App from './app';
import CategoryRoute from './handlers/category/category.route';
import WallpaperRoute from  './handlers/wallpaper/wallpaper.route';

const app = new App(
    [
        new CategoryRoute(),
        new WallpaperRoute(),
    ],
);

app.listen();
