import { Router } from 'express';
import { Joi } from 'celebrate';
import IController from '../../interfaces/controller.interface';
import CategoryController from './category.controller';
import authMiddleware from '../../middlewares/auth.middleware';
import validationMiddleware from '../../middlewares/validation.middleware';

class CategoryRoute implements IController {
    public router = Router();

    private categoryController = new CategoryController();

    constructor() {
        this.initializeRoutes();
    }

    private initializeRoutes() {
        this.router.get(
            '/categories',
            this.categoryController.getCategories,
        );
    }
}

export default CategoryRoute;