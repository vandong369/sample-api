import httpStatus = require('http-status');
import { NextFunction } from 'express';
import IRequestWithUser from '../../interfaces/requestWithUser.interface';
import IResponse from '../../interfaces/responseWithHelper.interface';

export default class CategoryController {
    public getCategories = async (req: IRequestWithUser, res: IResponse, next: NextFunction): Promise<any> => {
        try {
            const categories = [
                {
                    id: "1",
                    img: "https://image.com/img1.png"
                },
                {
                    id: "2",
                    img: "https://image.com/img2.png"
                }
            ]

            return res.success(httpStatus.OK, categories);
        } catch (err) {
            return next(err);
        }
    };
}
