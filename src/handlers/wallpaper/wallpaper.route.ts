import { Router } from 'express';
import { Joi } from 'celebrate';
import IController from '../../interfaces/controller.interface';
import WallpaperController from './wallpaper.controller';
import authMiddleware from '../../middlewares/auth.middleware';
import validationMiddleware from '../../middlewares/validation.middleware';

class WallpaperRoute implements IController {
    public router = Router();

    private wallpaperController = new WallpaperController();

    constructor() {
        this.initializeRoutes();
    }

    private initializeRoutes() {
        this.router.get(
            '/wallpapers',
            validationMiddleware({
                query: {
                    category: Joi.string().required(),
                }
            }),
            this.wallpaperController.getWallpapers,
        );
    }
}

export default WallpaperRoute;