echo "pm2 stop all"
pm2 stop all
echo "git pull origin dev"
git pull origin dev
echo "npm install"
npm install
echo "npm run build"
npm run build
echo "pm2 start ./dist/index.js"
pm2 start ./dist/index.js --name sample-api-dev
